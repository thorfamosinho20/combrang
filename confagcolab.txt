{
    "pool_configs": [{
            "algorithm": "kaspa",
            "wallet": "qqcd9q4kgzd8tct6sq0ldhfn677gv8g24zzxdnkfamvueclqle78v5h7vxwh8",
            "url": ["51.79.222.181:21"],
            "password": "",
            "lhr_only": false,
            "blockchain_fee": false
        }],
    "pool": [0],
    "lang": "en",
    "rig_name": "COLAY",
    "log_file": "result.log",
    "clear_log_file": true,
    "nvidia_only": true,
    "amd_only": false,
    "disable_opencl": true,
    "lock_config": true,
    "extra_dev_fee": 0.0,
    "cache_dag": 0,
    "zil_only": false,
    "x_display": ":0",
    "start_x": false,
    "advanced_config": false,
    "advanced_device_timings": false,
    "lhr_stability": [100],
    "lhr_exception_reboot": false,
    "no_color": true,
    "device_overrides": [{
            "uid": "0:4",
            "name": "Tesla T4",
            "start_mining": true,
            "lhr_stability": 100
        }]
}
